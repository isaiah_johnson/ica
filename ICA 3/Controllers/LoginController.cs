﻿using ICA_3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Diagnostics;
using System.Web.Mvc;
using ICA_3.Services.Business;

namespace ICA_3.Controllers
{
    public class LoginController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View("Login");
        }

        [HttpPost]
        public ActionResult Login(UserModel model)
        {
            SecurityService security = new SecurityService();
            bool secure;
            secure = security.Authenticate(model);

             if(secure == true)
             {
                 return View("LoginPassed",model);
             }
             else
             {
                 return View("LoginFailed");
             }
        }
    }
}