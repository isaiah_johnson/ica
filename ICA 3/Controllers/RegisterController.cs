﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ICA_3.Services.Business;
using ICA_3.Models;

namespace ICA_3.Controllers
{
    public class RegisterController : Controller
    {

        [HttpGet]
        public ActionResult Index()
        {
            return View("Register");
        }

        [HttpPost]
        public ActionResult Register(UserModel user)
        {
            SecurityService security = new SecurityService();
            bool result = false;
            result = security.CreateUser(user);

            if(result == true)
            {
                return View("RegisterPassed");
            }
            else
            {
                return View("RegisterFailed");
            }
        }
    }
}