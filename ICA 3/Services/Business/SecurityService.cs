﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ICA_3.Models;
using ICA_3.Services.Data;

namespace ICA_3.Services.Business
{
    public class SecurityService
    {
        public bool Authenticate(UserModel user)
        {
            SecurityDAO service = new SecurityDAO();
            return service.FindByUser(user) ;
        }

        public bool CreateUser(UserModel user)
        {
            SecurityDAO service = new SecurityDAO();
            if (service.FindByUser(user) == false)
                return service.Create(user);
            else
                return false;
        }

    }
}