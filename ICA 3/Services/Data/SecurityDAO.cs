﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ICA_3.Models;
using System.Data.SqlClient;
using System.Data;
using System.Diagnostics;

namespace ICA_3.Services.Data
{
    public class SecurityDAO
    {
        public bool FindByUser(UserModel user)
        {
            bool result = false;

            string connection =
            "Data Source = (localdb)\\MSSQLLocalDB;" +
            "initial catalog = Test;" +
            "User Id = IJohnson;" +
            "Password = Steelers3; " +
            "Integrated Security = SSPI;";

            try
            {
                string query = "SELECT * FROM dbo.Users WHERE USERNAME=@Username AND PASSWORD=@Password";

                using (SqlConnection cn = new SqlConnection(connection))
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    
                    cmd.Parameters.Add("@Username", SqlDbType.VarChar, 50).Value = user.Username;
                    cmd.Parameters.Add("@Password", SqlDbType.VarChar, 50).Value = user.Password;

                    cn.Open();
                    
                    // Using a DataReader see if query returns any rows
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.HasRows)
                        result= true;
                    else
                        result= false;
                    // Close the connection
                    cn.Close();
                }
            }
            catch (SqlException e)
            {
                // TODO: should log exception and then throw a custom exception
                throw e;
            }
            return result;
        }

        public bool Create(UserModel user)
        {
            bool result = false;

            string connection =
            "Data Source = (localdb)\\MSSQLLocalDB;" +
            "initial catalog = Test;" +
            "User Id = IJohnson;" +
            "Password = Steelers3; " +
            "Integrated Security = SSPI;";

            try
            {
                // Setup INSERT query with parameters
                string query = "INSERT INTO dbo.Users (USERNAME, PASSWORD) VALUES (@Username, @Password)";

                // Create connection and command
                using (SqlConnection cn = new SqlConnection(connection))
                using (SqlCommand cmd = new SqlCommand(query, cn))
                {
                    // Set query parameters and their values
                    cmd.Parameters.Add("@Username", SqlDbType.VarChar, 50).Value = user.Username;
                    cmd.Parameters.Add("@Password", SqlDbType.VarChar, 50).Value = user.Password;

                    // Open the connection, execute INSERT, and close the connection
                    cn.Open();

                    int rows = cmd.ExecuteNonQuery();
                    if (rows == 1)
                        result = true;
                    else
                        result = false;
                    cn.Close();
                }

            }
            catch (SqlException e)
            {
                // TODO: should log exception and then throw a custom exception
                throw e;
            }

            // Return result of create
            return result;
        }

    }
}